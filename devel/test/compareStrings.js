const params = GetParams()
console.log(params)

if (params.length !== 2) {
  console.log('invalid params, requires 2')
  process.exit()
}

const classes = require('../../classes.js')
classes.initLogger((msg, name, lvl) => {
  console.log(name, msg)
})
const SUtils = new classes.StringUtils()

console.log('Match percent:')
console.log(SUtils.CompareString(params[0], params[1]))

function GetParams () {
  return process.argv.splice(2)
}
